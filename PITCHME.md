# Chin Up

---

## Exercise of

*I've Got You; You've Got Me by the Goatee'*

+++

@ul

- Number of students: the whole class.
- Two by two.
- The two students hold one another's chins with their left hands.
- Their right hands, held u near their partner's left cheek, is ready to slap (tap) the partner's face every time the partner makes a noise.
- At the end of the exercise, ask the students if their partner was enjoying themself or not.

@ulend

---

<table style="float: left; font-size: large">
  <tr class="fragment">
    <th>
    <br><br>
    </th>
  </tr>
  <tr class="fragment">
    <td>PAIDIA</td>
  </tr>
  <tr class="fragment">
    <td>Tumult<br>
    Agitation<br>
    Immoderate Laughter</td>
  </tr>
  <tr class="fragment">
    <td><br><br><br>
    </td>
  </tr>
  <tr class="fragment">
    <td>Kite-flying<br>
    Solitaire<br>
    Patience<br>
    Crossword Puzzles</td>
  </tr>
  <tr class="fragment">
    <td>LUDUS</td>
  </tr>
</table>

<table style="float: right; font-size: large">
  <tr class="fragment">
    <th class="fragment">AGON
    <br>
    (Competition)</th>
    <th class="fragment">ALEA
    <br>
    (Chance)</th>
    <th class="fragment">MIMICRY
    <br>
    (Simulation)</th>
    <th class="fragment">ILINX
    <br>
    (Vertigo)</th>
  </tr>
  <tr class="fragment">
    <td class="fragment">Racing
    <br>
    Wrestling
    <br>
    Etc.
    <br>
    Athletics
    <br>
    <br>
    </td>
    <td class="fragment">Counting-out rhymes
    <br>
    Heads or tails
    <br>
    </td>
    <td class="fragment">Children's initiations
    <br>
    Games of illusion
    <br>
    Tag, Arms
    <br>
    Masks, Disguises</td>
    <td class="fragment">Children "whirling"
    <br>
    Horseback riding
    <br>
    Swinging
    <br>
    Waltzing</td>
  </tr>
  <tr class="fragment">
    <td class="fragment">Boxing, Billiards<br>Fencing, Checkers<br>Football, Chess</td>
    <td class="fragment">Betting<br>Roulette</td>
    <td></td>
    <td class="fragment">Volador<br>Traveling carnivals<br>Skiing<br>Mountain climbing<br>Tightrope walking</td>
  </tr>
  <tr class="fragment">
    <td class="fragment">Contests, Sports<br>in general</td>
    <td class="fragment">Simple, complex<br>and continuing<br>lotteries*</td>
    <td class="fragment">Theater<br>Spectacles in<br>general</td>
    <td></td>
  </tr>
</table>

---

@fa[thumbs-up](Thank You!)
